# STAGE 1
FROM node:16-alpine as builder
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY --chown=node:node *.json ./

RUN npm config set unsafe-perm true
RUN npm install -g typescript
RUN npm install -g ts-node

USER node
RUN npm install
COPY --chown=node:node ./src ./src
COPY --chown=node:node *.ts ./

RUN npm run test:ci
RUN npm run build

# STAGE 2
FROM node:12-alpine
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./
USER node

RUN npm install --production
COPY --from=builder /home/node/app/dist ./dist

# COPY --chown=node:node .env .
# COPY --chown=node:node  /config ./config
# COPY --chown=node:node  /public ./public

# COPY --chown=node:node .sequelizerc .
# RUN npm run migrate
# RUN npx sequelize db:seed:all; exit 0
# RUN npm un sequelize-cli

EXPOSE 3333
ENTRYPOINT [ "npm", "start" ]