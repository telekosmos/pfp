import bodyParser from 'body-parser';
import express, { Application, NextFunction, Request, Response } from 'express';
import { signinHandler } from './signin'

const app: Application = express();
app.use(bodyParser.json())

app.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.send('Express server with TypeScript');
});

app.post('/signin', signinHandler)

export { app };