import bodyParser from 'body-parser';
import { fold } from 'fp-ts/Either';
import express, { Application, NextFunction, Request, Response, Router } from 'express';
import { Authenticator } from '../../domain/Authenticator'
import { Server } from 'http';

const timeLogger = (req: Request, res: Response, next: NextFunction) => {
  console.log('Time: ', Date.now())
  next()
}

class Handlers {
  static rootHandler(req: Request, res: Response, next: NextFunction): Response {
    return res.json({ ok: true })
  }

  static signinHandler(authService: Authenticator) {
    return (req: Request, res: Response, next: NextFunction) => {
      const responseUnauth = (res: Response): ((error: Error) => void) => error => res
        .status(401)
        .json({ error: error.message })
        .end();

      const responseToken = (res: Response): ((token: String) => void) => token => res.status(200).json({ token })

      const { username, password } = req.body
      return fold(responseUnauth(res), responseToken(res))(authService.signin(username, password))
    }
  }
}

export class HttpApp {
  app: Application

  constructor(private authController: Authenticator) {
    this.app = express();
    this.app.use(timeLogger);
    this.app.use(bodyParser.json());
    
    this.app.get('/', Handlers.rootHandler);
    this.app.post('/signin', Handlers.signinHandler(authController))
  }

  start(port: number): Server {
    return this.app.listen(port, () => {
      console.log(`Server is listening on port ${port}`);
    });
  }
}
