import { Authenticator } from '../../domain/Authenticator';
import { UserRepositoryAlgebra } from '../../domain/user/UserRepositoryAlgebra';
import { UserService } from '../../domain/user/UserService';
import { UserRepositoryInMemory } from '../../infrastructure/repositories/UserRepositoryInMemory';
import { HttpApp } from './HttpApp'

const userRepository: UserRepositoryAlgebra = new UserRepositoryInMemory();
const userService: UserService = new UserService(userRepository);
const signin: Authenticator = new Authenticator(userService);

const httpApp = new HttpApp(signin);

export { httpApp };