import { Either, fromOption } from "fp-ts/Either";
import * as O from 'fp-ts/Option';
import { pipe } from 'fp-ts/lib/function';
import { UserService } from "./user/UserService";
import { User } from './user/User'


export class Authenticator {
  userService: UserService

  constructor(userService: UserService) {
    this.userService = userService
  }

  generateToken(u: User): string {
    return `token**${u.username}`
  }

  signin(username: string, password: string): Either<Error, string> {
    const authenticated: Either<Error, string> = pipe(
      this.userService.getUser(username, password),
      O.map(this.generateToken),
      fromOption(() => new Error('Invalid username or password'))
    );
    return authenticated;
  }
}
