import { Option } from 'fp-ts/Option';
import { User } from './User';

export interface UserRepositoryAlgebra {
  find(username: string, password: string): Option<User>;
}
