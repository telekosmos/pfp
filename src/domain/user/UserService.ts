import { Option } from 'fp-ts/Option'
import { User } from './User'
import { UserRepositoryAlgebra } from "./UserRepositoryAlgebra";

/**
 * Adapter or port ???
 */
export class UserService {
  userRepo: UserRepositoryAlgebra;

  constructor(userRepo: UserRepositoryAlgebra) {
    this.userRepo = userRepo
  }

  getUser(username: string, password: string): Option<User> {
    return this.userRepo.find(username, password)
  }
}