import { findFirst } from 'fp-ts/Array';
import { Option } from 'fp-ts/Option'
import { User } from '../../domain/user/User'
import { UserRepositoryAlgebra } from '../../domain/user/UserRepositoryAlgebra'

export class UserRepositoryInMemory implements UserRepositoryAlgebra {
  private readonly users: Array<User> = [
    { username: 'kevin', password: 'trey35' },
    { username: 'kyrie', password: 'earthflatter' }
  ];

  find(username: string, password: string): Option<User> {
    return findFirst((u: User) => u.username === username && u.password === password)(this.users)  
  }
}