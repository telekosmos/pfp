import * as O from 'fp-ts/Option';
import { Either, fromOption, fold } from 'fp-ts/lib/Either';
import { findFirst } from 'fp-ts/Array';
import { pipe } from 'fp-ts/lib/function'
import { NextFunction, Request, Response } from 'express';

type User = {
  username: string;
  password: string
}

const users: Array<User> = [
  { username: 'kd', password: 'kdsecret' },
  { username: 'pg2', password: 'pg2secret' }
];

export const signin = (username: string, password: string): Either<Error, string> => {
  const authenticate = (username: string, password: string): O.Option<User> => 
    findFirst((u: User) => username === u.username && u.password === password)(users);

  const generateToken = (u: User) => `token**${u.username}`

  return pipe(
    authenticate(username, password),
    O.map(generateToken),
    fromOption(() => new Error('Invalid username or password'))
  )
};

const responseUnauth = (res: Response): ((error: Error) => void) => error => res
    .status(401)
    .json({ error: error.message })
    .end();

const responseToken = (res: Response): ((token: String) => void) => token => res.status(200).json({ token })

export const signinHandler = (req: Request, res: Response, next: NextFunction): void => {
  const { username, password }: User = req.body

  fold(responseUnauth(res), responseToken(res))(signin(username, password))
}
