import 'jest';
import request from 'supertest';

import { Authenticator } from '../../domain/Authenticator'
import { UserService } from '../../domain/user/UserService';
import { UserRepositoryInMemory } from '../../infrastructure/repositories/UserRepositoryInMemory';
import { HttpApp } from '../../apps/http/HttpApp';

// import { app } from '../../app'

describe('Applications', () => {
  
  describe('App', () => {
    const application = new HttpApp(new Authenticator(new UserService(new UserRepositoryInMemory())));

    it('should response root path', async () => {
      const resp = await request(application.app).get('/');
      expect(resp.status).toBe(200);
      expect(resp.body).toEqual({ ok: true });
    });

    it('should response token sucessful', async () => {
      const mockRequest = {
        username: 'kevin',
        password: 'trey35'
      };

      const response = await request(application.app).post('/signin').send(mockRequest);
      expect(response.statusCode).toBe(200);
      expect(response.body.token).toContain(`token**${mockRequest.username}`);
    });

    it('should response 401-Unauthorized', async () => {
      const mockPostBody = {
        username: 'Jalen',
        password: 'Abcd1234'
      };
      const response = await request(application.app).post('/signin').send(mockPostBody);
      expect(response.statusCode).toBe(401);
      expect(response.body.error).toContain('Invalid');
    });
  });
})
