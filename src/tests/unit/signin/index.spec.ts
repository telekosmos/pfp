import 'jest';
import { left, right } from 'fp-ts/Either';
// import { signin } from '../../../signin';

import { Authenticator } from '../../../domain/Authenticator'
import { UserService } from '../../../domain/user/UserService';
import { UserRepositoryInMemory } from '../../../infrastructure/repositories/UserRepositoryInMemory';


describe('Signin function', () => {
  let authenticate: Authenticator;
  
  beforeEach(() => {
    authenticate = new Authenticator(new UserService(new UserRepositoryInMemory()))
  })
  it('should get a 401-Unauthorized response', () => {
    const mockUsername: string = 'any';
    const mockPassword: string = 'secret';

    const result = authenticate.signin(mockUsername, mockPassword);
    expect(result).toStrictEqual(left(expect.any(Error)))
  });

  it('should get a token successfull response', () => {
    const mockUsername: string = 'kyrie';
    const mockPassword: string = 'earthflatter';

    const result = authenticate.signin(mockUsername, mockPassword);
    expect(result).toStrictEqual(right(`token**${mockUsername}`))
  });
})

export {}